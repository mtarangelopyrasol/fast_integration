package com.pyrasol.aaal.fast.util;

import java.io.Console;
import java.security.AccessController;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.security.auth.Subject;

import org.apache.log4j.Logger;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.collection.PageIterator;
import com.filenet.api.constants.ClassNames;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.constants.TypeID;
import com.filenet.api.core.Connection;
import com.filenet.api.core.Document;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.core.IndependentObject;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.ReferentialContainmentRelationship;
import com.filenet.api.core.VersionSeries;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.exception.ExceptionCode;
import com.filenet.api.property.Properties;
import com.filenet.api.property.Property;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.Id;
import com.filenet.api.util.UserContext;
import com.pyrasol.aaal.fast.impl.Config;
import filenet.vw.api.VWAttachment;
import filenet.vw.api.VWAttachmentType;
import filenet.vw.api.VWSession;

/**
 * Content Engine helper class
 */
public class CeUtil {

	public static Logger logger = Logger.getLogger(CeUtil.class);
	private ObjectStore ceObjectStore;
	private VWSession vwsession;
	
	/**
	 * constructor
	 */
	public CeUtil (String sUri, String sUser, String sPassword, String sObjectStore) {
		logger.debug("CeUtil@username/password(): sUri=" + sUri + ", sUser=" + sUser + ", sObjectStore=" + sObjectStore + "...");  // DEBUG
		Connection conn = Factory.Connection.getConnection(sUri);
		
		Subject subject = UserContext.createSubject(conn, sUser, sPassword, null);
		UserContext uc = UserContext.get();
		uc.pushSubject(subject);
		
		PropertyFilter pf = new PropertyFilter();
		pf.addIncludeProperty(0, null, null, "Name", null);
		pf.addIncludeProperty(0, null, null, "Id", null);
		Domain domain = Factory.Domain.fetchInstance(conn, null, pf);
		
		// If we get this far, we've successfully connected
		ceObjectStore = Factory.ObjectStore.fetchInstance(domain,  sObjectStore,  pf);		
		logger.debug("Successfully logged in to objStore " + sObjectStore + ", domain=" + domain.get_Name());  // DEBUG		
	}
	
	/**
	 * Alternate constructor: connect to CE with the PE component's security context
	 */
	public CeUtil (String fnpeCeUri, String sObjectStore) throws Exception {
		logger.debug("CeUtil@existing session: fnpeCeUri=" + fnpeCeUri + ", sObjectStore=" + sObjectStore + "...");  // DEBUG

		//getVWSession();
		//dumpRuntimeEnv();
		Connection conn = Factory.Connection.getConnection(fnpeCeUri);
		// logger.debug("  conn=" + conn + "...");  DEBUG
				
		Subject subject = Subject.getSubject(AccessController.getContext());
		// logger.debug("  subject=" + subject + "...");  // DEBUG
		UserContext uc = UserContext.get();
		uc.pushSubject(subject);
		
		PropertyFilter pf = new PropertyFilter();
		pf.addIncludeProperty(0, null, null, "Name", null);
		pf.addIncludeProperty(0, null, null, "Id", null);
		Domain domain = Factory.Domain.fetchInstance(conn, null, pf);
		
		// If we get this far, we've successfully connected
		ceObjectStore = Factory.ObjectStore.fetchInstance(domain,  sObjectStore,  pf);		
		logger.debug("Successfully logged in to objStore " + sObjectStore + ", domain=" + domain.get_Name() + ".");  // DEBUG		
	}

	/**
	 * Close connection to CE
	 */
	public void closeConnection () {
		logger.info ("closeConnection()...");		
		UserContext uc = UserContext.get();
		uc.popSubject ();
		ceObjectStore = null;
	}
	
	/**
	 * Create folder
	 */
	public Folder createFolder(String parentPath, String folderName, String folderClass) throws Exception {
		logger.debug("createFolder(parthPath=" + parentPath + ", folderName=" + folderName + ", folderClass=" + folderClass + ")...");
		Folder rootFolder = Factory.Folder.fetchInstance(ceObjectStore, parentPath, null);
		Folder subFolder = rootFolder.createSubFolder(folderName);
		subFolder.save(RefreshMode.REFRESH);
		if (folderClass != null) {
			subFolder.changeClass(folderClass);
			subFolder.save(RefreshMode.REFRESH);
		}
		return subFolder;
	}
	
	/**
	 * Dump properties for specified document (DEBUG)
	 */
	@SuppressWarnings("rawtypes")
	public String dumpProperties(Document document) throws Exception {
		logger.debug("dumpProperties(" + document +  ")...");
		Properties props = document.getProperties();
		StringBuilder sb = new StringBuilder();
		Iterator it = props.iterator();
		int i = 0;
		while (it.hasNext()) {
			Property prop = (Property)it.next();
			sb.append("prop[" + i + "]" + prop + "...\n");
			//sb.append("prop[" + i + "] name=" + prop.getPropertyName() + ", class=" + prop.getClass() + ", value=" + prop.getObjectValue().toString() + "\n...");
			i++;
		}
		return sb.toString();
	}
	
	/**
	 * Dump environment variables/system properties for current runtime (DEBUG)
	 */
	public void dumpRuntimeEnv () {
		System.out.println(">>dumpRuntimeEnv(): All properties:");
		System.getProperties().list(System.out);
		System.out.println("<<All properties.");

		System.out.println(">>env:");
		Map<String, String> envMap = System.getenv();
		for (String key: envMap.keySet()) {
			System.out.println("env[" + key + "]=" + envMap.get(key));
		}
		System.out.println("<<env.");		
	}

	/**
	 * Fetch CE document by IS F_Docnumber
	 */
	@SuppressWarnings("unchecked")
	public Document fetchDocByF_Docnumber(String f_docnumber) throws Exception {
		logger.debug("fetchDocByF_Docnumber(" + f_docnumber +  ")...");
		SearchScope search = new SearchScope (ceObjectStore);
		String sql = "Select * from Invoice where f_Docnumber =  " + f_docnumber;
		SearchSQL searchSQL = new SearchSQL(sql);
		DocumentSet documents = (DocumentSet)search.fetchObjects(searchSQL,  50,  null,  true);
		Iterator<Document> it = documents.iterator ();
		if (!it.hasNext()) {
			throw new Exception ("fetchDocByF_Docnumber(): " + f_docnumber + " not found");
		}
		Document document = (Document)it.next();
		return document;
	}
	
	/**
	 * Fetch CE document by DocID GUID
	 */
	public Document fetchDocByDocId(String sDocumentId) {
		logger.debug("fetchDocByDocId(" + sDocumentId +  ")...");
		com.filenet.api.util.Id docId = new Id(sDocumentId);
		Document document = Factory.Document.fetchInstance(ceObjectStore, docId,  null);
		return document;
	}
	
	/**
	 * Fetch CE document by vsid
	 */	
	public Document fetchDocByVersionSeries(String sVersionSeriesId) {
		logger.debug("fetchDocByVersionSeries(" + sVersionSeriesId + ")...");
		com.filenet.api.util.Id vsId = new Id(sVersionSeriesId);
		VersionSeries vs = Factory.VersionSeries.fetchInstance(ceObjectStore, vsId,  null);
		Document document = (Document)vs.get_CurrentVersion();
		return document;
	}
	
	/**
	 * File CE object in specified CE folder
	 */
	public void fileObject(String folderPath, IndependentObject p8Obj) throws Exception {
		Folder p8Folder = (Folder)ceObjectStore.fetchObject(ClassNames.FOLDER, folderPath, null);
		ReferentialContainmentRelationship rcr = 
			Factory.ReferentialContainmentRelationship.createInstance(ceObjectStore, ClassNames.DYNAMIC_REFERENTIAL_CONTAINMENT_RELATIONSHIP);
		rcr.set_Head(p8Obj);
		rcr.set_Tail(p8Folder);
		rcr.save(RefreshMode.NO_REFRESH);
	}
	
	
	/**
	 * Get CE document by vsid (forces round trip, rather than cache read; explicit property filter)
	 */	
	public Document getDocByVersionSeries(String sVersionSeriesId, PropertyFilter pf) {
		logger.debug("getDocByVersionSeries(" + sVersionSeriesId + ")...");
		Id vsId = new Id(sVersionSeriesId);
		VersionSeries vs = Factory.VersionSeries.fetchInstance(ceObjectStore, vsId, null);
		//Document document = (Document)vs.get_CurrentVersion();
		Id docId = ((Document)vs.get_CurrentVersion()).get_Id();
		Document document = Factory.Document.fetchInstance(ceObjectStore, docId, pf);

		return document;
	}
	
	/**
	 * Return object store instance
	 * @return
	 */
	public ObjectStore getObjectStore () {
		return ceObjectStore;
	}

	/**
	 * Get VWSession from runtime context
	 */
	public VWSession getVWSession() throws Exception {
		logger.debug(">>getVWSession(): " + vwsession + "...");
		if (vwsession == null) {
			Subject subject = Subject.getSubject(AccessController.getContext());
			logger.debug("  subject=" + subject + "...");
			Class<?> clazz = Class.forName("filenet.vw.api.VWSession");
			logger.debug("  clazz=" + clazz + "...");
			Set<?> creds = subject.getPrivateCredentials(clazz);
			if (creds != null) {
				Iterator<?> it = creds.iterator();
				if (it != null && it.hasNext()) {
					vwsession = (VWSession)it.next();
				}
			}
		}
		logger.debug("<<getVWSession(): " + ((vwsession == null) ? "null" : vwsession.getConnectionPointName()) + ".");  // DEBUG		
		return vwsession;
	}
	
	
	/**
	 * Read specified property; return null if not found
	 */
	public static String readProperty(Properties props, String symbolicName, TypeID propertyType) {
		String retval = "";
		try {
			Property prop = props.get(symbolicName);
			if (propertyType.equals(TypeID.STRING)) {
				retval = prop.getStringValue();
			} else if (propertyType.equals(TypeID.DATE)) {
				Date dt = prop.getDateTimeValue();
				if (dt != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
					retval = sdf.format(dt);
				}
			} else if (propertyType.equals(TypeID.LONG)) { 
				int iv =  prop.getInteger32Value();
				retval = Integer.toString(iv);
			} else {
				retval = prop.toString();				
			}
		} catch (EngineRuntimeException fnre) {
			if (fnre.getExceptionCode() != ExceptionCode.API_PROPERTY_NOT_IN_CACHE) {
				logger.error("readProperty(" + symbolicName + "): " + fnre.getMessage());
			}
		}
		System.out.print("readProperty(" + retval + ")");
		return (retval != null) ? retval : "";
	}
	
	/**
	 * Perform CE search; return IndependentObjectSet
	 */	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Object> search(String sql) throws Exception {
		SearchScope searchScope = new SearchScope(ceObjectStore);
		SearchSQL searchSQL = new SearchSQL();
		searchSQL.setDistinct();
		searchSQL.setQueryString(sql);
		IndependentObjectSet results = searchScope.fetchObjects(searchSQL, null, null, Boolean.TRUE);
		ArrayList resultSet = new ArrayList<IndependentObject>();
		PageIterator p = results.pageIterator();
		while (p.nextPage()) {
			for (Object o : p.getCurrentPage()) {
				resultSet.add(o);
			}
		}
		return resultSet;
	}
	
	/**
	 * Convert CE document to VWAttachment
	 */
	public VWAttachment mkAttachment(Document ceDocument) throws Exception {
		VWAttachment vwattachment = new VWAttachment();
		vwattachment.setType(VWAttachmentType.ATTACHMENT_TYPE_DOCUMENT);
		vwattachment.setId(ceDocument.get_VersionSeries().get_Id().toString());
		vwattachment.setAttachmentName(ceDocument.get_Name());
		vwattachment.setLibraryName(ceObjectStore.get_Name());
		//vwattachment.setLibraryType.VWLibraryType.LIBRARY_TYPE_CONTENT_ENGINE); Obsolete in current API?
		return vwattachment;
	}
	
	/**
	 * Unit test
	 * 
	 * EXAMPLE OUTPUT:
	 *   Logging on to CE...
	 *   readProperties@FileInputStream(c:/paul/kaps-integration/proj/KAPSComponent/kapscomponent.properties)...
	 *   Successfully logged in to objStore SOM, domain=P8Domain
	 *   Querying sample doc(s)...
	 *   ceUtil.search(select * from UPR_INBCORR where DocumentTitle like 'Test Doc%'), count=1...
	 *   doc=Test Doc, version=1.0, current version=1.0
	 *   Converting to VWAttachment...
	 *   Disconnectioning from ceUtil...
	 */
	@SuppressWarnings("rawtypes")
	public static void main(String[] args) {
		try {
			System.out.println("Logging on to CE...");
			Config config = Config.getInstance ();
			CeUtil ceUtil = new CeUtil(config.getCeUri(), config.getCeUser(), config.getCePassword(), config.getCeObjectStore());
			
			System.out.println("Querying sample doc(s)...");
			String sql = "select * from UPR_INBCORR where DocumentTitle like 'Test Doc%'";
			List<Object> searchResults = ceUtil.search(sql);
			System.out.println("ceUtil.search(" +  sql + "), count=" + searchResults.size() + "...");
			Iterator it = searchResults.iterator();
			Document doc = null;
			while (it.hasNext()) {
				doc = (Document)it.next();
				System.out.println("doc=" + doc.get_Name() + 
						", version=" + doc.get_MajorVersionNumber() + "." + doc.get_MinorVersionNumber() + 
						", current version=" + doc.get_CurrentVersion().get_MajorVersionNumber() + "." + doc.get_CurrentVersion().get_MinorVersionNumber());
			}		
			System.out.println("Converting to VWAttachment...");
			if (doc != null) {
				VWAttachment vwattachment = ceUtil.mkAttachment(doc);
				System.out.println("VWAttachment(" + vwattachment.getAttachmentName() + ") successfully generated...");

				Properties props = doc.getProperties();
				String systemAreaID = readProperty(props, "SystemAreaID", TypeID.STRING);
				String systemAreaKeyID = readProperty(props, "SystemAreaKeyID", TypeID.STRING); 
				String dateReceived = readProperty(props, "DateReceived", TypeID.STRING);
				String dateScanned = readProperty(props, "DateScanned", TypeID.STRING);
				String guid = doc.get_VersionSeries().get_Id().toString();
				System.out.println("systemAreaID=" + systemAreaID + ", systemAreaKeyID=" + systemAreaKeyID + 
					", dateReceived=" + dateReceived + ", dateScanned=" + dateScanned + ", guid=" + guid + "...");
			}			
			
			System.out.println("Disconnectioning from ceUtil...");
			ceUtil.closeConnection();
		}
		catch (Exception e) {
			System.out.println ("ERROR: " + e);
			e.printStackTrace();
		} 
	}

}
