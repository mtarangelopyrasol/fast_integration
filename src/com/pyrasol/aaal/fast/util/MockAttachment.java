package com.pyrasol.aaal.fast.util;

import com.filenet.api.core.Document;
import com.pyrasol.aaal.fast.impl.Config;

import filenet.vw.api.VWAttachment;

/**
 * Create dummy VW attachment (for static main testing)
 */
public class MockAttachment {

	public static VWAttachment getInstance (String guid) throws Exception {
		Config config = Config.getInstance();
		CeUtil ceUtil = new CeUtil(config.getCeUri(), config.getCeUser(), config.getCePassword(), config.getCeObjectStore());
		Document doc = ceUtil.fetchDocByDocId(guid);
		VWAttachment  vwattachment = ceUtil.mkAttachment(doc);
		ceUtil.closeConnection();
		ceUtil = null;
		return vwattachment;
	}
	
	
}
