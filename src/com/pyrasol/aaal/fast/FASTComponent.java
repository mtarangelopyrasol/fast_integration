package com.pyrasol.aaal.fast;

import org.apache.log4j.Logger;

import com.pyrasol.aaal.fast.impl.NotifyFAST;
import com.pyrasol.aaal.fast.impl.ReadFASTConfig;

import filenet.vw.api.VWAttachment;

/**
 * FASTComponent interface definitions
 */
public class FASTComponent {

	protected static Logger logger = Logger.getLogger(FASTComponent.class);
	
	/**
	 * Write message to log4j
	 */
	public void log(int severity, String message) {
		/*
		 * REFERENCE:
		 * https://logging.apache.org/log4j/2.0/log4j-core/apidocs/org/apache/logging/log4j/core/net/Severity.html
		 * <= 7 severity levels, including 0) System unusable, 3) Error, 4) Warning, 6) Informational, 7) Debug
		 */
		switch (severity) {
		case 0:
			logger.error(message);
			break;
		case 1:
			logger.warn(message);
			break;
		case 2:
			logger.info(message);
			break;
		case 3:
			logger.debug(message);
			break;			
		default:
			logger.warn("Unknown severity(" + severity + "), message: " + message);
		}		
	}

	/**
	 * Notify FAST
	 */
	public String[] notifyFAST(String action, VWAttachment doc) throws Exception {
		return new NotifyFAST().doNotify(action, doc);
	}
	
	/**
	 * Read configuration parameters
	 */
	public String[] readFASTConfig(String[] parameters) throws Exception {
		return new ReadFASTConfig().read(parameters);
		
	}
}
