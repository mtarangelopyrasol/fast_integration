package com.pyrasol.aaal.fast.impl;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Provides runtime access to FAST configuration
 */
public class Config {
	
	public static Logger logger = Logger.getLogger(Config.class);

	// Configuration file name (OPTIONAL) DFASTCOMPONENT  
	public static final String PROPS_FILE = "fastcomponent.properties";
	public static final String PROPS_DEFINE = "FASTCOMPONENT_PROPERTIES_PATH"; 
	
	private static Config fastComponentConfig;
	
	private String ceUri;
	private String ceUser;
	private String cePassword;
	private String ceObjectStore="CMTOS";
	private String fastUrl;
	private String maxRetries;
	private String delayMinutes;
	private String fnpeCeUri = "iiop://localhost:2809/FileNet/Engine";
	
	/**
	 * return singleton instance to client
	 */
	public static Config getInstance () throws Exception {
		if (fastComponentConfig == null) {
			fastComponentConfig = new Config ();
			logger.info("config: " + fastComponentConfig.dumpConfig());
		}
		return fastComponentConfig;
	}
	
	/**
	 * private singleton constructor
	 */
	private Config() throws Exception {
		Properties props = readProperties();
		if (props.getProperty("ceUri") != null)
			ceUri = props.getProperty("ceUri");
		if (props.getProperty("ceUser") != null)
			ceUser = props.getProperty("ceUser");
		if (props.getProperty("cePassword") != null)
			cePassword = props.getProperty("cePassword");
		if (props.getProperty("ceObjectStore") != null)
			ceObjectStore = props.getProperty("ceObjectStore");
		if (props.getProperty("fastUrl") != null)
			fastUrl = props.getProperty("fastUrl");
		if (props.getProperty("maxRetries") != null)
			maxRetries = props.getProperty("maxRetries");
		if (props.getProperty("delayMinutes") != null)
			delayMinutes = props.getProperty("delayMinutes");
		if (props.getProperty("fnpeCeUri") != null)
			fnpeCeUri = props.getProperty("fnpeCeUri");
	}

	/**
	 * Dump selected config items (DEBUG)
	 */
	public String dumpConfig() {
		StringBuilder sb = new StringBuilder();		
		sb.append("ceObjectStore: " + this.getCeObjectStore() + "\n");
		sb.append("fastUrl: " + this.fastUrl + "\n");
		sb.append("maxRetries: " + this.maxRetries + "\n");
		sb.append("delayMinutes: " + this.delayMinutes + "\n");
		sb.append("fnpeCeUri: " + this.fnpeCeUri);
		return sb.toString();		
	}

	/**
	 * Read properties from .jar, or from custom location
	 */
	private Properties readProperties () throws Exception {
		Properties props = new Properties ();
		InputStream is = null;
		
		String filePath = System.getProperty(PROPS_DEFINE);
		if (filePath != null) {
			// Use custom properties filepath
			try {
				logger.debug("Config.readProperties@FileInputStream(" + filePath + ")...");  // DEBUG			
				is = new FileInputStream (filePath);
				props.load(is);
				is.close ();
			} catch (Exception e) {
				String errText = "fastComponentConfig: Unable to open properties file using System Property (" + filePath + "): " + e.getMessage();
				logger.error(errText);
				throw new Exception(errText);
			}
		} else {
			logger.warn("WARNING: " + PROPS_DEFINE + " is not defined: trying to read " + PROPS_FILE + " instead...");  // DEBUG			
			try {
				logger.debug("Config.readProperties@getClass().getClassLoader().getResourceAsStream(" + PROPS_FILE + ")...");  // DEBUG			
				// Read properties from .jar
				is = this.getClass().getClassLoader().getResourceAsStream (PROPS_FILE);
				props.load(is);
				is.close ();
			} catch (Exception e) {
				String errText = "fastComponentConfig: Unable to open properties file in JAR (" + PROPS_FILE + "): " + e.getMessage();
				logger.error(errText);
				throw new Exception(errText);
			}
		}
		
		return props;
	}

	/**
	 * public getter methods
	 */
	public String getCeUri () { return ceUri; }
	public String getCeUser () { return ceUser; }
	public String getCePassword () { return cePassword; }
	public String getCeObjectStore () { return ceObjectStore; }	
	public String getFastUrl () { return fastUrl; }	
	public String getMaxRetries () { return maxRetries; }	
	public String getDelayMinutes () { return delayMinutes; }
	public String getFnpeCeUri() { return fnpeCeUri; }
		
	/**
	 * Unit test
	 * 
	 * EXAMPLE OUTPUT:
	 *   getCeUri=http://ecmdemo1:9080/wsi/FNCEWS40MTOM
	 *   getCeUser=p8admin
	 *   getCePassword=filenet
	 *   getCeObjectStore=CMTOS
	 */
	public static void main(String[] args) {
		System.out.println("user.dir=" + System.getProperty("user.dir") + "...");
		try {
			Config config = Config.getInstance ();
			System.out.println(System.getProperty(PROPS_DEFINE));
			System.out.println("getCeUri=" + config.getCeUri());
			System.out.println("getCeUser=" + config.getCeUser());
			System.out.println("getCePassword=" + config.getCePassword());
			System.out.println("getCeObjectStore=" + config.getCeObjectStore());
			System.out.println("getFastUrl=" + config.getFastUrl());
			System.out.println("getMaxRetries=" + config.getMaxRetries());
			System.out.println("getDelayMinutes=" + config.getDelayMinutes());
			System.out.println("fnpeCeUri=" + config.getFnpeCeUri());
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}

}
