package com.pyrasol.aaal.fast.impl;

/**
 * Provides a mechanism for a workflow to read FAST configuration properties file
 */
public class ReadFASTConfig {

	/**
	 * Read specified configuration parameters, return in values[] array
	 */
	public String[] read(String[] parameters) throws Exception {
		String[] values = new String[parameters.length];
		Config config = Config.getInstance();
		for (int i=0; i < parameters.length; i++) {
			String p = parameters[i];
			if (p.equalsIgnoreCase("maxRetries")) {
				values[i] = config.getMaxRetries();
			} else if (p.equalsIgnoreCase("delayMinutes")) {
				values[i] = config.getDelayMinutes();
			} else {
				values[i] = "";
			}
		}
		return values;
	}
	
	/**
	 * Unit test
	 */
	public static void main(String[] args) {
		try {
			ReadFASTConfig readFASTConfig= new ReadFASTConfig();
			String[] parameters = {"cow", "maxRetries", "delayMinutes" };
			String[] values = readFASTConfig.read(parameters);
			for (int i=0; i < parameters.length; i++) {
				System.out.println("parameters[" + parameters[i] + "]=" + values[i]);
			}			
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}

}
