package com.pyrasol.aaal.fast.impl;

import java.io.BufferedReader;
import java.io.Console;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.TypeID;
import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;
import com.filenet.api.property.PropertyFilter;
import com.pyrasol.aaal.fast.util.CeUtil;
import com.pyrasol.aaal.fast.util.MockAttachment;

import filenet.vw.api.VWAttachment;

/**
 * NotifyKAPS (implementation)
 */
public class NotifyFAST {
	
	protected static Logger logger = Logger.getLogger(NotifyFAST.class);
	private boolean debug = false;
	//private boolean debug = true;
	
	/**
	 * Parse VWAttachment with respect to specified action and notify KAPS
	 */
	public String[] doNotify(String action, VWAttachment doc) throws Exception {
		logger.debug("doNotify(action=" + action + ", doc=" + doc.getAttachmentName() + ")...");

		String xml = generateMessage(action, doc);
		String[] retval = sendNotificationMsg(xml);
		return retval;
	}

	/**
	 * Build notification message from input parameters in XML. 
	 */
	public String[] doNotify2(String action, String guid, String systemAreaID, String systemAreaKeyID, String dateReceived, String dateScanned) throws Exception {
		logger.debug("doNotify2()...");

		String notificationMessage = 
				"  <Document>\n" +
				"    <Action>'ADD'</Action>\n" +
				"    <PolicyNumber>{0}</PolicyNumber>\n" +
				"    <DocumentType>{0}</DocumentType>\n" +
				"    <DocumentDescription>{0}</DocumentDescription>\n" +
				"    <DocumentDetail>{0}</DocumentDetail>\n" +
				"    <ObjectIdentifier>{0}</ObjectIdentifier> \n" +
				"    <VersionSeriesIdentifier>{0}</VersionSeriesIdentifier> \n" +
				"  </Document>";

		String[] retval = sendNotificationMsg(notificationMessage);
		return retval;
	}
	
	/**
	 * Read document properties to build JSON notification message
	 */
	private String generateMessage(String action, VWAttachment vwattachment) throws Exception {
		logger.debug("generateMessage(action=" + action + ", vwattachment=" + vwattachment.getAttachmentName() + ")..."); // DEBUG		
		
		String notificationMessage = null; 
		try {
			// Connect to CE
			Config config = Config.getInstance();
			CeUtil ceUtil = null;
			if (debug)
				ceUtil = new CeUtil(config.getCeUri(), config.getCeUser(), config.getCePassword(), config.getCeObjectStore());
			else
				ceUtil = new CeUtil(config.getFnpeCeUri(), config.getCeObjectStore());
			
			// Read attachment properties
			if (!action.equals("DELETE")) {
				PropertyFilter pf = new PropertyFilter();
				pf.addIncludeProperty(0, null, null, PropertyNames.VERSION_SERIES, 1);
				pf.addIncludeProperty(0, null, null, "A88", 1);
				pf.addIncludeProperty(0, null, null, "ipdDocTypeId", 1);
				pf.addIncludeProperty(0, null, null, "A50", 1);
				pf.addIncludeProperty(0, null, null, "E24", 1);
				pf.addIncludeProperty(0, null, null, "A3", 1);
				Document document= ceUtil.getDocByVersionSeries(vwattachment.getId(), pf);
				//logger.debug("properties=" + ceUtil.dumpProperties(document));  // DEBUG
			
				Properties props = document.getProperties();			
				String policyNumber = CeUtil.readProperty(props, "A88", TypeID.STRING);
				String documentType = CeUtil.readProperty(props, "ipdDocTypeId", TypeID.STRING); 
				String documentDescription = CeUtil.readProperty(props, "A50", TypeID.LONG);
				String documentDetail = CeUtil.readProperty(props, "E24", TypeID.STRING);
				String archiveOnly = CeUtil.readProperty(props, "A3", TypeID.LONG);
				String guid = vwattachment.getId();
				logger.debug("Doc properties: Policy Number=" + policyNumber + ", Document Type=" + documentType + ", Document Description=" + documentDescription + ", Document Detail=" + documentDetail + ", guid=" + guid + "...");
		
				// Build message
				notificationMessage = 
					"  <Document>\n" +
							"    <Action>" + action + "</Action>\n" +
							"    <PolicyNumber>" + policyNumber + "</PolicyNumber>\n" +
							"    <DocumentType>" + documentType + "</DocumentType>\n" +
							"    <DocumentDescription>" + documentDescription + "</DocumentDescription>\n" +
							"    <DocumentDetail>" + documentDetail + "</DocumentDetail>\n" +
							"    <ObjectIdentifier>" + guid + "</ObjectIdentifier> \n" +
							"    <ArchiveOnly>" + archiveOnly + "</ArchiveOnly> \n" +
							"    <VersionSeriesIdentifier></VersionSeriesIdentifier> \n" +
							"  </Document>";
			} else {
				// Build "delete" message
				String guid = vwattachment.getId();
				logger.debug("guid=" + guid + "...");
				notificationMessage = 
					"  <Document>\n" +
							"    <Action>'ADD'</Action>\n" +
							"    <PolicyNumber></PolicyNumber>\n" +
							"    <DocumentType></DocumentType>\n" +
							"    <DocumentDescription></DocumentDescription>\n" +
							"    <DocumentDetail></DocumentDetail>\n" +
							"    <ObjectIdentifier></ObjectIdentifier> \n" +
							"    <VersionSeriesIdentifier></VersionSeriesIdentifier> \n" +
							"  </Document>";
			}
		} catch (Exception e) {
			String errText = "generateMessage error: " + e.getMessage();
			logger.error(errText, e);
			throw new Exception(errText);
		}
		logger.debug("notificationMessage: " + notificationMessage);		
		return notificationMessage;
	}
	
	/**
	 * Open HTTP(S) connection and send notification message to FAST
	 */
	private String[] sendNotificationMsg(String notificationMessage) throws Exception {
		logger.debug("sendNotificationMsg()...");  // DEBUG
		List<String> responseText = new ArrayList<String>();
		try {
			// Setup connection
			String url = Config.getInstance().getFastUrl();
			HttpURLConnection conn = null;
			conn = (HttpURLConnection) new URL(url).openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0");
			conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			conn.setRequestProperty("Content-Type", "application/xml");
		
			// Send post request
			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(notificationMessage);
			wr.flush();
			wr.close();

			// Return HTTP status code as 1st element in array.  "200" indicates "success"
			int responseCode = conn.getResponseCode();
			responseText.add(Integer.toString(responseCode));
			logger.debug("Response Code: " + responseCode);  // DEBUG

			// Read remaining text (if any) a line at a time
			BufferedReader in = new BufferedReader(
				new InputStreamReader(conn.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				responseText.add(inputLine);
				logger.debug(" responseText[" + (responseText.size()-1) + "]=" + inputLine + "...");  // DEBUG
			}
			in.close();
		
		} catch (Exception e) {
			String errText = "sendNotificationMsg error: " + e.getMessage();
			logger.error(errText, e);
			throw new Exception(errText);
		}

		// Convert REST response text to String[] array
		// NOTE: "List.toArray(new String[0])" is actually about twice as fast as "List.toArray(new String[myString.size()])"
		String[] retval = responseText.toArray(new String[0]);
		return retval;
	}
	
	/**
	 * Unit test
	 */
	public static void main(String[] args) {
		System.out.println("NotifyFAST...");
		try {
			NotifyFAST notifyFAST = new NotifyFAST();
			notifyFAST.debug = true;  // DEBUG
			VWAttachment vwattachment = MockAttachment.getInstance("{05CCD4DC-5E6D-4256-B25F-7F093C24B4F2}");
			String[] retval = notifyFAST.doNotify("ADD", vwattachment);
			System.out.println("retval[] length=" + retval.length);
			for (int i=0; i < retval.length; i++) {
				System.out.println("  retval[" + i + "]=" + retval[i]);
			}
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
